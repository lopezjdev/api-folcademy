package com.myfirstapi2.Apifolcademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiFolcademyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiFolcademyApplication.class, args);
	}

}
